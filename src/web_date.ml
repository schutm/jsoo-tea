open Js_of_ocaml

type t = <
> Js.t


type date_obj = <
  now : unit -> float [@bs.meth];
> Js.t


external create_date : unit -> t = "Date" [@@bs.new]

let date_obj = Js.Unsafe.global##.Date

let now () = date_obj##now ()
