open Js_of_ocaml

type t = <
  length : int Js.readonly_prop;
  clear : unit -> unit Js.meth;
  key : int -> string Js.meth;
  getItem : string -> string Js.opt Js.meth;
  removeItem : string -> unit Js.meth;
  setItem : string -> string -> unit Js.meth;
> Js.t

let localStorage window =
  try Js.Optdef.to_option window##.localStorage
  with _ -> None

let length window = match localStorage window with
  | None -> None
  | Some localStorage -> Some (localStorage##.length)


let clear window = match localStorage window with
  | None -> None
  | Some localStorage -> Some (localStorage##clear ())


let key window idx = match localStorage window with
  | None -> None
  | Some localStorage -> Some (localStorage##key idx)


let getItem window key = match localStorage window with
  | None -> None
  | Some localStorage ->
    try Some (localStorage##getItem key)
    with _ -> None


let removeItem window key = match localStorage window with
  | None -> None
  | Some localStorage -> Some (localStorage##removeItem key)


let setItem window key value = match localStorage window with
  | None -> None
  | Some localStorage -> Some (localStorage##setItem key value)
