# JSOO-TEA

Proof of concept [Js_of_ocaml](https://ocsigen.org/js_of_ocaml/3.1.0/manual/overview) port of
[Bucklescript-tea[(https://github.com/OvermindDL1/bucklescript-tea).

Only the test directory is compilable with ```opam exec -- dune build```. Afterwards open the ```index.html```
in your browser.
