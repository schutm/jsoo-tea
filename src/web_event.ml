open Js_of_ocaml

(* class type target = object
  method value : string Js.optdef Js.readonly_prop
end *)

type 'node t = <
  target : 'node Js.optdef Js.readonly_prop;
  keyCode : int [@bs.get];
  preventDefault : unit -> unit Js.meth;
  stopPropagation : unit -> unit Js.meth;
> Js.t

type 'node cb = 'node t -> unit [@bs]

type options = bool (* false | true (* TODO:  Define a javascript record as another option *) *)


type popstateEvent = <
> Js.t

type popstateCb = popstateEvent -> unit [@bs]
