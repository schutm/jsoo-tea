open Js_of_ocaml

class type style = object
   method setProperty : Web_json.t Js.optdef Js.readonly_prop (* TODO:  Revamp this and the next line... *)
   method setProperty__ : string -> string Js.opt -> string Js.opt -> unit Js.meth
end

let getStyle = Js.Unsafe.get

let setStyle = Js.Unsafe.set

type t = <
  style : style Js.t Js.readonly_prop;
  value : string Js.optdef Js.prop;
  checked : bool Js.optdef Js.prop;
  childNodes : t Js.js_array Js.t Js.readonly_prop;
  firstChild : t Js.opt Js.readonly_prop;
  appendChild : t -> t Js.meth;
  removeChild : t -> t Js.meth;
  insertBefore : t -> t -> t Js.meth;
  remove : unit -> unit Js.meth;
  setAttributeNS : string -> string -> string -> unit Js.meth;
  setAttribute : string -> string -> unit Js.meth;
  removeAttributeNS : string -> string -> unit Js.meth;
  removeAttribute : string -> unit Js.meth;
  addEventListener : string -> t Web_event.cb -> Web_event.options -> unit Js.meth;
  removeEventListener : string -> t Web_event.cb -> Web_event.options -> unit Js.meth;
  focus : unit -> unit Js.meth;
  (* Text Nodes only *)
  nodeValue : string Js.prop;
> Js.t

let document_node = Js.Unsafe.global##.document

type event = t Web_event.t

type event_cb = t Web_event.cb

let setProp = Js.Unsafe.set

(* external getProp_asEventListener : t -> 'key -> t Web_event.cb Js.optdef = "" [@@bs.get_index]

external setProp_asEventListener : t -> 'key -> t Web_event.cb Js.optdef -> unit = "" [@@bs.set_index]

external getProp : t -> 'key -> 'value = "" [@@bs.get_index]

external setProp : t -> 'key -> 'value -> unit = "" [@@bs.set_index] *)

let style n = n##.style

let getStyle n key = getStyle n##.style key

let setStyle n key value = setStyle n##.style key value

let setStyleProperty n ?(_priority=false) key value =
  setStyle n key value
(*  match Js.Optdef.test ((Js.Unsafe.coerce style)##.setProperty) with
  match Js.Optdef.to_option style##.setProperty with
  | None -> setStyle n key value (* TODO:  Change this to setAttribute sometime, maybe... *)
  | Some _valid -> style##setProperty__ key value (if priority then (Js.Opt.return "important") else Js.Opt.empty)
*)
let childNodes n = n##.childNodes

let firstChild n = n##.firstChild

let appendChild n child = n##appendChild child

let removeChild n child = n##removeChild child

let insertBefore n child refNode = n##insertBefore child refNode

let remove n child = n##remove child

let setAttributeNS n namespace key value = n##setAttributeNS namespace key value

let setAttribute n key value = n##setAttribute key value

let setAttributeNsOptional n namespace key value =
  match namespace with
  | "" -> n##setAttribute key value
  | ns -> n##setAttributeNS ns key value

let removeAttributeNS n namespace key = n##removeAttributeNS namespace key

let removeAttribute n key = n##removeAttribute key

let removeAttributeNsOptional n namespace key =
  match namespace with
  | "" -> n##removeAttribute key
  | ns -> n##removeAttributeNS ns key

let addEventListener n typ listener options = n##addEventListener typ listener options

let removeEventListener n typ listener options = n##removeEventListener typ listener options

let focus n = n##focus ()

(* Text Nodes only *)

let set_nodeValue n text = n##.nodeValue := text

let get_nodeValue n = n##.nodeValue


(* Polyfills *)

let remove_polyfill : unit -> unit = fun () ->
  Js.Unsafe.pure_js_expr {|
  // remove polyfill
  (function() {
    if (!('remove' in Element.prototype)) {
      Element.prototype.remove = function() {
        if (this.parentNode) {
          this.parentNode.removeChild(this);
        }
      };
    };
  }())
  |}
