(*let counter = let open Test_client_counter in main
let counter_debug_beginner = let open Test_client_counter_debug_beginner in main
let counter_debug_standard = let open Test_client_counter_debug_standard in main
let counter_debug_program = let open Test_client_counter_debug_program in main
let btn_update_span = let open Test_client_btn_update_span in main
let attribute_removal = let open Test_client_attribute_removal in main
let drag = let open Test_client_drag in main
let on_with_options = let open Test_client_on_with_options in main
let http_task = let open Test_client_http_task in main *)

open Js_of_ocaml

let start = Test_client_counter.main

let _ =
  let root = Web.Document.getElementById("root") in
  Dom_html.window##.onload :=
    Dom_html.handler (fun _ ->
        ignore (start root ());
        Js._false)
