open Js_of_ocaml

class type _formdata = object
  method append : string -> string -> unit Js.meth
  (* method append_blob : string -> Web_blob.t -> string -> unit *)
end [@bs]
type t = _formdata Js.t

let create () : t = Js.Unsafe.global##._FormData

let append key value f = f##append key value
