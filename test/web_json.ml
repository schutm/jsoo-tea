open Js_of_ocaml
(* include Js.Json
*)

type t

type 'a kind =
  | String: Js.js_string kind
  | Number: float kind
  | Object: t Jstable.t kind
  | Array: t array kind
  | Boolean: bool kind
  | Null: unit kind

type tagged_t =
  | JSONFalse
  | JSONTrue
  | JSONNull
  | JSONString of string
  | JSONNumber of float
  | JSONObject of t Map.Make(String).t
  | JSONArray of t Js.js_array

let classify (value) =
    (* let string_constr : Js.js_string Js.t Js.constr = Js.Unsafe.global##._String in
    let array_constr : 'a Js.js_array Js.t Js.constr = Js.Unsafe.global##._Array in *)
    (* let object_constr : t Jstable.t Js.t Js.constr = Js.Unsafe.global##._Object in *)
    (* if Js.instanceof value string_constr then (JSONString (Obj.magic value))
    else if Js.instanceof value array_constr then (JSONArray (Obj.magic value)) *)
    JSONObject (Obj.magic value)

(*include Deriving_Json

type nothingYet
external stringify : 't -> nothingYet Js.opt -> int -> string = "JSON.stringify" [@@bs.val]
*)

let string_of_json value =
  match Js.Optdef.to_option value with
  | None -> "undefined"
  | Some v ->
    try Json.output v |> Js.to_string
    with _ -> ""
(* 
let of_type (type a) (_v : a kind) (x : a) : t =
  x |> Obj.magic *)
  (* typeof *)
    

(*
let null : Js_types.null_val = Obj.magic Js.null
*)
