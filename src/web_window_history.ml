open Js_of_ocaml

type t = <
  length : int [@bs.get];
  back : unit -> unit [@bs.meth];
  forward : unit -> unit [@bs.meth];
  go : int -> unit [@bs.meth];
  pushState : t -> string -> string -> unit [@bs.meth];
  replaceState : t -> string -> string -> unit [@bs.meth];
  state : t [@bs.get];
> Js.t


let length window = match Js.Optdef.to_option window##history with
  | None -> -1
  | Some history -> history##length

let back window = match Js.Optdef.to_option window##history with
  | None -> ()
  | Some history -> history##back

let forward window = match Js.Optdef.to_option window##history with
  | None -> ()
  | Some history -> history##forward

let go window to' = match Js.Optdef.to_option window##history with
  | None -> ()
  | Some history -> history##go to'

let pushState window state title url = match Js.Optdef.to_option window##history with
  | None -> ()
  | Some history -> history##pushState state title url

let replaceState window state title url = match Js.Optdef.to_option window##history with
  | None -> ()
  | Some history -> history##replaceState state title url

let state window = match Js.Optdef.to_option window##history with
  | None -> Js.Optdef.empty
  | Some history -> history##state
