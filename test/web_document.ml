open Js_of_ocaml
(* TODO:  Polyfill document if it is missing, like on node or in native *)

type t = <
  body : Web_node.t Js.readonly_prop;
  createElement : string -> Web_node.t Js.meth;
  createElementNS : string -> string -> Web_node.t Js.meth;
  createComment : string -> Web_node.t Js.meth;
  createTextNode : string -> Web_node.t Js.meth;
  getElementById : string -> Web_node.t Js.opt (* Js.optdef *) Js.meth;
  location : Web_location.t Js.readonly_prop;
> Js.t

let document = Js.Unsafe.global##.document

let body () = document##.body

let createElement typ = document##createElement typ

let createElementNS namespace key = document##createElementNS namespace key

let createComment text = document##createComment text

let createTextNode text = document##createTextNode text

let getElementById id = document##getElementById id

let createElementNsOptional namespace tagName =
  match namespace with
  | "" -> document##createElement tagName
  | ns -> document##createElementNS ns tagName

let location () = document##.location
